#!/usr/bin/python3

import sys
import string
import requests

URL = "https://bnv.web.ctfcompetition.com/api/search"

lst = {}

def main(text):
    global lst 
    lst = create_dic()
    txt = list(map(lookup, text))
    txt = ''.join(str(e) for e in txt)
    send(txt)

def lookup(ch):
    return lst[str(ch)] * 10

def send(msg):
    param = {"message":"{}".format(msg)}
    r = requests.post(url = URL, json = param )
    data = r.json()
    print(data)

def create_dic():

    lst = list(string.ascii_lowercase)
    lst.extend(['1','2', '3', '4', '5','6','7','8','9','0',",",";",":",".","?","!","\'","\"","-","%","=", " "])
    val_lst = [1,12,14,145,15,124,1245,125,24,245,13,123,134,1345,135,1234,12345,1235,234,2345,136,1236,2456,1346,13456,1356,34561,345612,345614,3456145,345615,3456124,34561245,3456125,345624,3456245,2,23,25,256,236,235,3,32356,36,251456,52356,00]
    lst = dict(zip(lst, val_lst))
    return lst



if __name__ == "__main__":
    main(sys.argv[1])
